import { IncomingMessage } from "http";
import { parse } from "url";
import { ParsedRequest } from "./types";

export function parseRequest(req: IncomingMessage) {
  console.log("HTTP " + req.url);
  const { pathname, query } = parse(req.url || "/", true);
  const { avatar, theme } = query || {};

  if (Array.isArray(avatar)) {
    throw new Error("Expected a single avatar");
  }

  if (Array.isArray(theme)) {
    throw new Error("Expected a single theme");
  }

  const arr = (pathname || "/").slice(1).split(".");
  let text = "";
  if (arr.length === 0) {
    text = "";
  } else if (arr.length === 1) {
    text = arr[0];
  } else {
    text = arr.join(".");
  }

  const parsedRequest: ParsedRequest = {
    text: decodeURIComponent(text),
    avatar: avatar || `https://avatar.tobi.sh/${text}`,
    theme: theme === "dark" ? "dark" : "light",
  };
  return parsedRequest;
}
