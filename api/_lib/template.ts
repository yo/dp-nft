import { readFileSync } from "fs";
import { sanitizeHtml } from "./sanitizer";
import { ParsedRequest } from "./types";
const twemoji = require("twemoji");
const twOptions = { folder: "svg", ext: ".svg" };
const emojify = (text: string) => twemoji.parse(text, twOptions);

const rglr = readFileSync(`${__dirname}/../_fonts/Devparty.woff`).toString(
  "base64"
);

function getCss(theme: string) {
  let background = "white";
  let foreground = "black";
  let radial = "lightgray";

  if (theme === "dark") {
    background = "black";
    foreground = "white";
    radial = "dimgray";
  }

  return `
@font-face {
  font-family: 'Inter';
  font-style:  normal;
  font-weight: normal;
  src: url(data:font/woff2;charset=utf-8;base64,${rglr}) format('woff2');
}
body {
  background: ${background};
  font-family: 'Inter', sans-serif;
  background-image: radial-gradient(circle at 25px 25px, ${radial} 2%, transparent 0%), radial-gradient(circle at 75px 75px, ${radial} 2%, transparent 0%);
  background-size: 100px 100px;
  height: 100vh;
  display: flex;
  text-align: center;
  align-items: center;
  justify-content: center;
  margin: 0 100px;
}
.emoji {
  height: 1em;
  width: 1em;
  margin: 0 .05em 0 .1em;
  vertical-align: -0.1em;
}
.heading {
  width: 100%;
  font-size: 80px;
  font-style: normal;
  color: ${foreground};
  line-height: 1.8;
}
.avatar {
  margin-bottom: 80px;
  border-radius: 100px;
  border: 6px solid #d2d2d2;
  margin-right: 20px;
  height: 180px;
  width: 180px;
}
`;
}

export function getHtml(parsedReq: ParsedRequest) {
  const { text, theme, avatar } = parsedReq;
  return `
<!DOCTYPE html>
<html>
  <meta charset="utf-8">
  <title>NFT</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>${getCss(theme)}</style>
  <body>
    <div>
      <div class="user-block">
        <img class="avatar" src=${avatar} />
      </div>
      <div class="heading">${emojify(sanitizeHtml(text))}</div>
    </div>
  </body>
</html>
`;
}
