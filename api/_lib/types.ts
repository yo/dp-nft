export type Theme = "light" | "dark";

export interface ParsedRequest {
  text: string;
  avatar: string;
  theme: Theme;
}
